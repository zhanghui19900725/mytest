/**
 * Bestpay.com.cn Inc.
 * Copyright (c) 2011-2018 All Rights Reserved.
 */
package network.TCP;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author yinShiPeng
 * @version Id: Server.java, v 0.1 2018/10/2 23:40 yinShiPeng Exp $$
 */
public class Server implements Runnable {
    private ServerSocket serverSocket;

    public Server(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        serverSocket.setSoTimeout(10000);
    }
    public void run() {
        //循环语句使服务器一直等待连接
        while (true){
            try {
                System.out.println("等待远程连接，端口号为："+serverSocket.getLocalPort()+"...");
                Socket server = serverSocket.accept();
                System.out.println("远程主机地址："+server.getRemoteSocketAddress());
                DataInputStream in = new DataInputStream(server.getInputStream());
                System.out.println("收到客户端内容为："+in.readUTF());
                DataOutputStream out = new DataOutputStream(server.getOutputStream());
                out.writeUTF("谢谢你连接我："+server.getLocalAddress()+"\nGoodbye!");
                server.close();
            } catch (IOException e) {
                e.printStackTrace();
                System.exit(0);
            }
        }
    }

    public static void main(String[] args) throws IOException {
        Server server = new Server(1234);
        Thread threadServer = new Thread(server);
        threadServer.run();
    }
}
